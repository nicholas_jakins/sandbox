# VisualizeWords - Sandbox

This project can be used to run the Visualize Words web application 
locally. 

At a high level this project is comprised of a Spring Boot application 
and an Angular front-end UI.  

The Spring Boot server exposes a REST API for processing given words in a text grid. 
The server uses [Redis](https://redis.io/) for session and user management so that 
each user has it's own data. 

The Angular front-end has support for integrating with the server's REST API 
and allows users to register a username and begin entering words in the text area.  

See related repositories [words-vizualizer](https://bitbucket.org/nicholas_jakins/words-vizualizer/src/master/)
and [text-processor](https://bitbucket.org/nicholas_jakins/text-processor/src/master/).

To run you will need to do the following:

You will need docker-compose installed and an account with docker 
hub. 

Run `sh run_words_visualizer.sh` to start the run the containers 
after having logged into [dockerhub](https://hub.docker.com/). 
The images are public, so you should be able to run them locally.

To access the web application navigate to `http://localhost:8080` 
in your browser.

![demo](demo.gif)



